﻿using UnityEngine;
using System.Collections;

public class MouseRotate : MonoBehaviour {

    public float XSensitivity = 1f;

    public float YSensitivity = 1f;

    private float xRotation;
    private float yRotation;

	
	void Start ()
    {
        xRotation = transform.rotation.eulerAngles.x;
        yRotation = transform.rotation.eulerAngles.y;
    }
	
	
	void Update ()
    {
        float xValue = Input.GetAxis("Mouse X") * XSensitivity;
        float yValue = Input.GetAxis("Mouse Y") * YSensitivity;
        yRotation += xValue;
        xRotation += yValue;
        transform.rotation = Quaternion.Euler(xRotation, yRotation, 0);
	}
}
