﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class JumpPad : MonoBehaviour {

	public Vector3 ForceDirection;

	public float Force = 10;

	public Transform ParticleEffectRoot;


	void Start () {
	
	}
	

	void Update () {
	
		ParticleEffectRoot.transform.rotation = Quaternion.LookRotation (ForceDirection);
	}

	void OnDrawGizmos() {
		Gizmos.color = Color.red;
		Gizmos.DrawRay (transform.position, ForceDirection);
	}

	void OnTriggerEnter(Collider other) 
	{
		
		if (other.gameObject.name == "Player") 
		{
			Debug.Log (other.gameObject.name);
			other.GetComponent<Rigidbody> ().AddForce (ForceDirection.normalized * Force, ForceMode.Impulse);
		}
	}
}
