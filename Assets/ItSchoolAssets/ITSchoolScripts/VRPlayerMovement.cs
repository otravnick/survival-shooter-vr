﻿using UnityEngine;
using System.Collections;

public class VRPlayerMovement : MonoBehaviour
{
    [SerializeField]
    private float moveSpeed = 1f;

    private Rigidbody playerRigidBody;
	
	void Start () {
        playerRigidBody = GetComponent<Rigidbody>();
    }
	
	
	void FixedUpdate ()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        Vector3 moveDirection = (transform.forward * vertical + transform.right * horizontal).normalized;
        Vector3 moveVector = moveDirection * moveSpeed;
        Vector3 movePosition = transform.position + moveVector;
        playerRigidBody.MovePosition(movePosition);
	}
}
