﻿using UnityEngine;
using System.Collections;

public class BloodVignette : MonoBehaviour {

	public Material ProcessMaterial;

	public void SetVignettePower(float power)
	{
		ProcessMaterial.SetFloat ("_Power", power);
	}

	void OnRenderImage(RenderTexture src, RenderTexture dest)
	{
		Graphics.Blit(src, dest, ProcessMaterial);
	}


}
