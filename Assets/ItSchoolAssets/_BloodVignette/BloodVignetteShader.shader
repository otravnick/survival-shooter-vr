﻿Shader "CustomPostEffects/BloodVignetteShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_VignetteTexture ("VignetteTexture", 2D) = "white" {}
		_Power ("Power", Range(0,1)) = 0
		_TintColor ("TintColor", Color) = (1,0,0,1)
	}
	SubShader
	{
		Tags
		{
			"RenderType" = "Opaque"
		}

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			sampler2D _MainTex;
			sampler2D _VignetteTexture;
			float4 _TintColor;
			float _Power;



			struct VertexInput
			{
				float4 position : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct VertexOutput
			{
				float2 uv : TEXCOORD0;
				float4 position : SV_POSITION;
			};

			VertexOutput vert (VertexInput v)
			{
				VertexOutput o;
				o.position = mul(UNITY_MATRIX_MVP, v.position);
				o.uv = v.uv;
				return o;
			}

			fixed4 frag (VertexOutput i) : COLOR
			{
				float4 mainTexRGB = tex2D(_MainTex, i.uv);
				float4 vignetteRGB = tex2D(_VignetteTexture, i.uv);
				float vignettePower = vignetteRGB.a * _Power;
				float3 emissive = lerp(mainTexRGB.rgb, _TintColor.rgb,vignettePower);
				return fixed4(emissive, 1);
			}
			ENDCG
		}
	}
}