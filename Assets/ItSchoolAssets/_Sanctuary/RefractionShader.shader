// Shader created with Shader Forge v1.28 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.28;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:32909,y:32655,varname:node_3138,prsc:2|emission-1625-RGB;n:type:ShaderForge.SFN_Tex2d,id:5654,x:31862,y:32530,ptovrint:False,ptlb:RefractionTexture,ptin:_RefractionTexture,varname:node_5654,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:dd2b176c8f25f044a895ca8de795f28e,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Append,id:7748,x:32147,y:32543,varname:node_7748,prsc:2|A-5654-R,B-5654-G;n:type:ShaderForge.SFN_Slider,id:4409,x:31837,y:33010,ptovrint:False,ptlb:RefractionPower,ptin:_RefractionPower,varname:node_4409,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_SceneColor,id:1625,x:32618,y:32824,varname:node_1625,prsc:2|UVIN-1547-OUT;n:type:ShaderForge.SFN_ScreenPos,id:7271,x:32285,y:32904,varname:node_7271,prsc:2,sctp:2;n:type:ShaderForge.SFN_Add,id:1547,x:32520,y:32654,varname:node_1547,prsc:2|A-9845-OUT,B-7271-UVOUT;n:type:ShaderForge.SFN_Multiply,id:9845,x:32313,y:32682,varname:node_9845,prsc:2|A-7748-OUT,B-4409-OUT;proporder:5654-4409;pass:END;sub:END;*/

Shader "Shader Forge/RefractionShader" {
    Properties {
        _RefractionTexture ("RefractionTexture", 2D) = "bump" {}
        _RefractionPower ("RefractionPower", Range(0, 1)) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        GrabPass{ }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _GrabTexture;
            uniform sampler2D _RefractionTexture; uniform float4 _RefractionTexture_ST;
            uniform float _RefractionPower;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 screenPos : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                o.screenPos = o.pos;
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5;
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
////// Lighting:
////// Emissive:
                float3 _RefractionTexture_var = UnpackNormal(tex2D(_RefractionTexture,TRANSFORM_TEX(i.uv0, _RefractionTexture)));
                float3 emissive = tex2D( _GrabTexture, ((float2(_RefractionTexture_var.r,_RefractionTexture_var.g)*_RefractionPower)+sceneUVs.rg)).rgb;
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
